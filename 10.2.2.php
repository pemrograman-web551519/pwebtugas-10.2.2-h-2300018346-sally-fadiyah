<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>For Berkalang</title>
</head>
<body>
    <pre>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $tinggi = intval($_POST["tinggi"]);
    for ($baris = 1; $baris <= $tinggi; $baris++) {
        // Print spaces
        for ($i = 1; $i <= $tinggi - $baris; $i++) {
            echo " ";
        }
        // Print stars
        for ($j = 1; $j < 2 * $baris; $j++) {
            echo "*";
        }
        echo "\n";
    }
} else {
?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        Tinggi: <input type="number" name="tinggi" min="1" required>
        <input type="submit" value="Submit">
    </form>
<?php
}
?>
    </pre>
</body>
</html>
